plugin.tx_teufels_cpt_cnt_bs_tab {
    settings {
        production {
            includePath {
                public = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.includePath.public}
                private = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.includePath.private}
                frontend {
                    public = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.includePath.frontend/public}
                }
            }
            bootstrapTab {
                sClassOuter = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.bootstrapTab.sClassOuter}
                sClassInner = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.bootstrapTab.sClassInner}
                sContainerElement = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.bootstrapTab.sContainerElement}
            }
        }
    }
}

page {
    includeCSS {
        10400_bootstrapTab_less_0 = {$plugin.tx_teufels_cpt_cnt_bs_tab.settings.production.includePath.private}Assets/Less/bootstrapTab.less
    }
}