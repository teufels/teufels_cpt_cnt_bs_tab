<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'teufels_cpt_cnt_bs_tab');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('TEUFELS.TeufelsCptCntBsTab', 'Content');